#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <random>
#include "passwd-utils.hpp"
#include "sha256.h"

std::string generate_password(int length, unsigned long long seed) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0, sizeof(alphanum) - 2);

    std::string password;
    for (int i = 0; i < length; ++i) {
        password += alphanum[distribution(generator)];
    }
    return password;
}

std::unordered_map<std::string, std::string> load_rainbow_table(const std::string& filename) {
    std::unordered_map<std::string, std::string> rainbow_table;
    std::ifstream file(filename);
    std::string plaintext, hash, reduced;
    while (file >> plaintext >> hash >> reduced) {
        rainbow_table[hash] = plaintext;
    }
    return rainbow_table;
}

std::string reduce(const std::string& hash, int length) {
    unsigned long long value = 0;
    for (char c : hash) {
        value = (value * 256 + c);
    }
    return generate_password(length, value);
}

void crack_passwords(const std::string& input_filename, const std::string& output_filename, const std::unordered_map<std::string, std::string>& rainbow_table) {
    std::ifstream input_file(input_filename);
    std::ofstream output_file(output_filename);
    if (!input_file || !output_file) {
        std::cerr << "Failed to open file." << std::endl;
        return;
    }

    std::string hash;
    while (std::getline(input_file, hash)) {
        if (hash.empty()) {
            continue;
        }
        auto it = rainbow_table.find(hash);
        if (it != rainbow_table.end()) {
            output_file << it->second << std::endl;
            output_file.flush();
        } else {
            output_file << "?" << std::endl;
            output_file.flush();
        }
    }

    output_file << std::endl;
    std::cout << "Password cracking is done. Check the output file for results." << std::endl;
}

int main() {
    std::string rainbow_table_filename, input_filename, output_filename;
    std::cout << "Enter the name of the rainbow table file: ";
    std::cin >> rainbow_table_filename;
    std::cout << "Enter the name of the input file with hashes: ";
    std::cin >> input_filename;
    std::cout << "Enter the name of the output file for cracked passwords: ";
    std::cin >> output_filename;

    auto rainbow_table = load_rainbow_table(rainbow_table_filename);
    crack_passwords(input_filename, output_filename, rainbow_table);
    return 0;
}