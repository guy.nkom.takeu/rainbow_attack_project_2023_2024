# rainbow_attack_project_2023_2024
This is a rainbow attack project done with c++ and other libraries explained in the readme file.

# Getting started
Before we start, once more we present our group members : 

- [ ] EYAGA NDEME Solange Dominique : 006557692
- [ ] MEGHADIO GONZEU Cedric Jordan : 000564114
- [ ] WANDJA Ghislain Geovani : 000507001
- [ ] TIANOU TCHEKOUYEP Franck Vaneck : 00561039
- [ ] NKOM TAKEU Guy Junior : 000562542

To make it easy for you to get started with the project, we will recommend you to have the following requirements first : 


# Requirements

- [ ] Ubuntu 22 
- [ ] SQLite
- [ ] GCC
- [ ] Source code

Command to install SQLite library for c++ in ubuntu : 
`sudo apt-get install sqlite3 libsqlite3-dev`

Now lets get to the installation phase

# Installation (Using the scripts)

During this phase, you will begin by cloning the project using the ssh url sent to you by mail :

```
cd folder_name
git clone ssh_url_of_project
cd rainbow_attack_project_2023_2024
```
**Remark:** Since you are using ssh url, make sure you configure SSH and the public keys correctly on your local machine, else the project is public and could be cloned using the https url below : 

[https://gitlab.com/guy.nkom.takeu/rainbow_attack_project_2023_2024.git](https://gitlab.com/guy.nkom.takeu/rainbow_attack_project_2023_2024.git)

The next phase concerns the manipulation of the scripts

# How to use the scripts

Using the terminal, making sure we are in the directory of rainbow project we cloned earlier, we are going to type the following commands : 

## Rainbow table generation

In order to generate the rainbow table, we have to define the number of characters of the passwords and the name of the generated file. This is an illustration : 

- [ ] Here we type the command to generate the rainbow table :

```
./generate-table
```
You will be asked to enter the number of characters of passwords and next the name of the rainbow table file. 

**Hints :** 
- You could name your rainbow table file like this : `rainbow-table-6.txt` with 6 representing the number of characters for passwords with 6 characters. 
- Rename the rainbow table files with respect to the number of characters of passwords to make it easy for you during the attacking phase

**Remark :** 
The script used to generate the rainbow table is so fast and efficient that it could generate a file of 10Go or more in less than one hour but we respected the constraint of generating a file less than 12Go

Now lets get to the attacking phase.

## How to use the attack script
The attack script uses the rainbow table file generated in the previous steps to crack the passwords. Given the fact that the rainbow table file is too large, the computational power needed to use this file is really high. In order to solve this problem, we used a process of indexation to facilitate the research, that is, SQLite.

**Remark :** 
- In order to use the attack script, you need the rainbow table file generated above, the file containing the hash values of the passwords corresponding to the number of characters you used to generate the rainbow table. 
- Please ! do not confuse on the number of characters because it is very important for the good functionning of the attack

This is the attacking process : In order to launch the attack script, type this command in the terminal :

- Make sur you install the library for SQlite before launching the attack : `sudo apt-get install sqlite3 libsqlite3-dev`

```
./crack-password-2
```


- [ ] You will be asked to enter the name of the rainbow table file you wish to use to carry out the attack ;
- [ ] Next you will be asked to enter the name of the file containing the hash values of the passwords you wish to crack ;
- [ ] Finally, you will asked to enter the name of the file that will contain the cracked passwords thus the output. Then, the attack will begin. 

**Very Important !** : 
- Make sure all the files are found in the same directory
- The rainbow table file used in the attack should correspond to the number of characters of the passwords used for the attack
- The input file, that is the hash values should correspond to the passwords with the number of characters used in the rainbow table
- The best deal is to generate various rainbow table files from 6 to 10 characters and later on use them during the attack.

At the end of the attack, a file will be created containing the passwords cracked in the directory of the project.

Enjoy our work !


