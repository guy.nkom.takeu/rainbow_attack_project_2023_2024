#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <sqlite3.h>
#include "passwd-utils.hpp"
#include "sha256.h"

// Function to generate a random alphanumeric password
std::string generate_password(int length, unsigned long long seed) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0, sizeof(alphanum) - 2);

    std::string password;
    for (int i = 0; i < length; ++i) {
        password += alphanum[distribution(generator)];
    }
    return password;
}

// Reduction function
std::string reduce(const std::string& hash, int length) {
    unsigned long long value = 0;
    for (char c : hash) {
        value = (value * 256 + c);
    }
    return generate_password(length, value);
}

// Crack the passwords
void crack_passwords(const std::string& input_filename, const std::string& output_filename, sqlite3* db) {
    std::ifstream input_file(input_filename);
    std::ofstream output_file(output_filename);
    if (!input_file || !output_file) {
        std::cerr << "Failed to open file." << std::endl;
        return;
    }

    std::string hash;
    while (std::getline(input_file, hash)) {
        if (hash.empty()) {
            continue;
        }
        std::string sql = "SELECT plaintext FROM rainbow_table WHERE hash = '" + hash + "'";
        sqlite3_stmt* stmt;
        int rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
        if (rc != SQLITE_OK) {
            std::cerr << "Failed to fetch data: " << sqlite3_errmsg(db) << std::endl;
            return;
        }

        rc = sqlite3_step(stmt);
        if (rc == SQLITE_ROW) {
            const unsigned char* plaintext = sqlite3_column_text(stmt, 0);
            output_file << plaintext << std::endl;
        } else if (rc == SQLITE_DONE) {
            output_file << "?" << std::endl;
        } else {
            std::cerr << "Failed to step: " << sqlite3_errmsg(db) << std::endl;
            return;
        }
        sqlite3_finalize(stmt);
    }

    output_file << std::endl;
    std::cout << "Password cracking is done. Check the output file for results." << std::endl;
}

int main() {
    std::string rainbow_table_filename, input_filename, output_filename;
    std::cout << "Enter the name of the rainbow table file: ";
    std::cin >> rainbow_table_filename;
    std::cout << "Enter the name of the input file with hashes: ";
    std::cin >> input_filename;
    std::cout << "Enter the name of the output file for cracked passwords: ";
    std::cin >> output_filename;

    sqlite3 *db;
    int rc = sqlite3_open(":memory:", &db);
    if (rc) {
        std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return(0);
    }

    char *errMsg = 0;
    std::string sql = "CREATE TABLE rainbow_table (hash TEXT, plaintext TEXT)";
    rc = sqlite3_exec(db, sql.c_str(), NULL, NULL, &errMsg);
    if (rc != SQLITE_OK) {
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return 1;
    }

    std::ifstream file(rainbow_table_filename);
    std::string plaintext, hash, reduced;
    //char *errMsg = 0;

    rc = sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &errMsg);
    if (rc != SQLITE_OK) {
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return 1;
    }

    while (file >> plaintext >> hash >> reduced) {
        size_t pos = plaintext.find("'");
        while (pos != std::string::npos) {
        plaintext.replace(pos, 1, "''");
        pos = plaintext.find("'", pos + 2);
        }
        std::string sql = "INSERT INTO rainbow_table (hash, plaintext) VALUES ('" + hash + "', '" + plaintext + "')";
        rc = sqlite3_exec(db, sql.c_str(), NULL, NULL, &errMsg);
        if (rc != SQLITE_OK) {
            std::cerr << "SQL error: " << errMsg << std::endl;
            sqlite3_free(errMsg);
            return 1;
        }
    }

    rc = sqlite3_exec(db, "COMMIT TRANSACTION", NULL, NULL, &errMsg);
    if (rc != SQLITE_OK) {
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return 1;
    }

    crack_passwords(input_filename, output_filename, db);

    sqlite3_close(db);
    return 0;
}