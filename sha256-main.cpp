#include <iostream>
#include "sha256.h"
#include "staticstring.hpp"

using std::string;
using std::cout;
using std::endl;
 
int main(int argc, char *argv[])
{   
    string input1 = "fender";        
    string output1 = sha256(input1);
    cout << "sha256('"<< input1 << "'):" << output1 << endl;
    
    StaticString<10> input2 = "barney";
    StaticString<65> output2 = sha256(input2);
    cout << "sha256('"<< input2 << "'):" << output2 << endl;

    string input3 = "please";        
    string output3 = sha256(input3);
    cout << "sha256('"<< input3 << "'):" << output3 << endl;

    string input4 = "brandy";        
    string output4 = sha256(input4);
    cout << "sha256('"<< input4 << "'):" << output4 << endl;

    StaticString<10> input5 = "badboy";
    StaticString<65> output5 = sha256(input5);
    cout << "sha256('"<< input5 << "'):" << output5 << endl;

    StaticString<10> input6 = "iwantu";
    StaticString<65> output6 = sha256(input6);
    cout << "sha256('"<< input6 << "'):" << output6 << endl;

    StaticString<10> input7 = "slayer";
    StaticString<65> output7 = sha256(input7);
    cout << "sha256('"<< input7 << "'):" << output7 << endl;

    StaticString<10> input8 = "flower";
    StaticString<65> output8 = sha256(input8);
    cout << "sha256('"<< input8 << "'):" << output8 << endl;

    StaticString<10> input9 = "rabbit";
    StaticString<65> output9 = sha256(input9);
    cout << "sha256('"<< input9 << "'):" << output9 << endl;

    StaticString<10> input10 = "wizard";
    StaticString<65> output10 = sha256(input10);
    cout << "sha256('"<< input10 << "'):" << output10 << endl;

    return 0;
}
