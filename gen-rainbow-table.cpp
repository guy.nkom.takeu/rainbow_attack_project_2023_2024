#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <random>
#include "passwd-utils.hpp"
#include "sha256.h" // Include the header file for sha256.cpp

// Function to generate a random alphanumeric password
std::string generate_password(int length, unsigned int seed) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0, sizeof(alphanum) - 2);

    std::string password;
    for (int i = 0; i < length; ++i) {
        password += alphanum[distribution(generator)];
    }
    return password;
}

// Load dictionary from file
std::vector<std::string> load_dictionary(int length) {
    std::string filename = "dict_" + std::to_string(length) + ".txt";
    std::vector<std::string> dictionary;
    std::ifstream file(filename);
    std::string line;
    while (std::getline(file, line)) {
        dictionary.push_back(line);
    }
    return dictionary;
}

// Reduction function
std::string reduce(const std::string& hash, int length) {
    unsigned long long value = 0;
    for (char c : hash) {
        value = (value * 256 + c);
    }
    return generate_password(length, value);
}

void generate_rainbow_table(int length, const std::string& filename) {
    std::vector<std::string> dictionary = load_dictionary(length);
    std::ofstream file(filename);
    if (!file) {
        std::cerr << "Failed to open file: " << filename << std::endl;
        return;
    }

    std::cout << "Rainbow table of passwords with " << length << " characters is generating ..." << std::endl;
    // Generate the rainbow table for dictionary entries.
    for (const std::string& plaintext : dictionary) {
        std::string hash_value = sha256(plaintext); // Use the sha256 function from sha256.cpp
        std::string reduced = reduce(hash_value, length);
        file << plaintext << " " << hash_value << " " << reduced << std::endl;
    }

    // Generate the rainbow table for random passwords.
    // Assuming we can generate and hash 1000 passwords per second, we can generate about 28,800,000 passwords in 8 hours.
    for (unsigned long long i = 0; i < 130000000; ++i) {
        //100 000 000 passwords for 10Go 
        std::string plaintext = generate_password(length, i);
        std::string hash_value = sha256(plaintext); // Use the sha256 function from sha256.cpp
        std::string reduced = reduce(hash_value, length);
        file << plaintext << " " << hash_value << " " << reduced << std::endl;
    }

    file.close();
    std::cout << "Rainbow table file is generated !" << std::endl;
}

int main() {
    int length;
    std::string filename;
    std::cout << "Enter the length of the password (between 6 and 10): ";
    std::cin >> length;
    if (length < 6 || length > 10) {
        std::cerr << "Invalid password length. It should be between 6 and 10." << std::endl;
        return 1;
    }
    std::cout << "Enter the name of the rainbow table file: ";
    std::cin >> filename;
    generate_rainbow_table(length, filename);
    return 0;
}